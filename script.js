function calculateMargin() {
  $( ".imgbox" ).each(function() {
    var new_m, row_q, row_w;
    var m = 10;
    var boxw = $(this).innerWidth(); //box width
    var imgw = $(this).children()[0].width; //assume all images have the same width
    var q = $(this).children().length; //how many images there are in the box
    var totalw = (imgw + m) * q; //total width of the images and minimum margin (M)
    if(boxw<totalw) {
      row_q = Math.floor(boxw/(imgw+m)); //how many images fit per line
      row_w = row_q*(imgw+m); //row width per line, in pixels
      new_m = ((boxw-row_w)/row_q +m)/2; //new left and right margin
      $("img", this).css("margin-left",new_m+"px");
      $("img", this).css("margin-right",new_m+"px");
    }
    else {
      $("img", this).css("margin-left","5px");
      $("img", this).css("margin-right","5px");
    }
  });
}
window.onload = calculateMargin();

window.onresize = function(event) {
  calculateMargin();
}